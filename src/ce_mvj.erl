% @doc
% Chess in Erlang move Judge (Judge determines whether or not
% something is legal)
-module(ce_mvj).

-export([
    is_legal/3
]).

-type board() :: cherl:board().
-type fr()    :: cherl:fr().


%%% API

-spec is_legal(fr(), fr(), board()) -> legal
                                     | {illegal, Reasons :: [Reason :: any()]}.
% @doc
% Checks if a move is legal or not; if not, returns a list of reasons
% why it is illegal
% @end

% for now, just do a bounds check
is_legal(SrcFR, DstFR, Board) ->
    Checks = [{fun srcfr_in_bounds/1, [SrcFR]},
              {fun dstfr_in_bounds/1, [DstFR]},
              {fun src_has_piece/2, [SrcFR, Board]}],
    check_fold(Checks, []).



%%% PRIVATE

-spec check_fold(Checks :: [Check], Accum :: Reasons) -> Result
    when Check   :: {Fun :: fun(), Args :: [any()]},
         Result  :: legal | {illegal, Reasons},
         Reasons :: [Reason :: any()].
% @private
% Fold over a list of checks, returning any illegalities found in the
% potential move
% @end

% at the end of the checks, no errors, say legal
check_fold([], []) ->
    legal;
check_fold([], Reasons) ->
    {illegal, Reasons};
check_fold([{ChkFn, ChkArgs} | Rest], ReasonsAcc) ->
    NewReasonsAcc =
        case erlang:apply(ChkFn, ChkArgs) of
            halal              -> ReasonsAcc;
            {haram, NewReason} -> [NewReason | ReasonsAcc]
        end,
    check_fold(Rest, NewReasonsAcc).



-spec dstfr_in_bounds(SrcFR :: fr()) -> halal | {haram, Reason :: any()}.
% @private
% Checks if the destination coordinate (given as a file/rank) is in
% bounds or not.

dstfr_in_bounds({fr, F, R}) when a =< F, F =< h,
                                 1 =< R, R =< 8 ->
    halal;
dstfr_in_bounds(_) ->
    {haram, destination_fr_out_of_bounds}.



-spec src_has_piece(SrcFR :: fr(), Board :: board()) -> halal | {haram, Reason :: any()}.
% @private
% Makes sure there is actually a piece in the source square

src_has_piece(SrcFR, Board) ->
    case cherl:frth(SrcFR, Board) of
        {p, _, _} -> halal;
        empty     -> {haram, source_square_is_empty}
    end.



-spec srcfr_in_bounds(SrcFR :: fr()) -> halal | {haram, Reason :: any()}.
% @private
% Makes sure the source is a real square

srcfr_in_bounds({fr, F, R}) when a =< F, F =< h,
                                 1 =< R, R =< 8 ->
    halal;
srcfr_in_bounds(_) ->
    {haram, source_fr_out_of_bounds}.
