-module(cherl).

-export_type([
    board/0,
    fr/0
]).
-export([
    default_board/0,
    frth/2,
    mv/3,
    pf_board_wb/1,
    pp_board_wb/1
]).

-type color() :: white | black.
-type caste() :: pawn
               | rook
               | knight
               | bishop
               | queen
               | king.
-record(p,
        {color :: color(),
         caste :: caste()}).
-type piece() :: #p{}.
-type file()  :: a | b | c | d | e | f | g | h.
-type rank()  :: 1..8.
-type fr()    :: {fr, file(), rank()}.
-type board() :: #{fr() := piece()}.
-type square() :: piece() | empty.


-spec default_board() -> board().
% @doc
% The default way a chess board is laid out at the beginning of a
% game.

default_board() ->
    #{{fr, a, 1} => {p, white, rook},
      {fr, b, 1} => {p, white, knight},
      {fr, c, 1} => {p, white, bishop},
      {fr, d, 1} => {p, white, queen},
      {fr, e, 1} => {p, white, king},
      {fr, f, 1} => {p, white, bishop},
      {fr, g, 1} => {p, white, knight},
      {fr, h, 1} => {p, white, rook},
      {fr, a, 2} => {p, white, pawn},
      {fr, b, 2} => {p, white, pawn},
      {fr, c, 2} => {p, white, pawn},
      {fr, d, 2} => {p, white, pawn},
      {fr, e, 2} => {p, white, pawn},
      {fr, f, 2} => {p, white, pawn},
      {fr, g, 2} => {p, white, pawn},
      {fr, h, 2} => {p, white, pawn},
      {fr, a, 7} => {p, black, pawn},
      {fr, b, 7} => {p, black, pawn},
      {fr, c, 7} => {p, black, pawn},
      {fr, d, 7} => {p, black, pawn},
      {fr, e, 7} => {p, black, pawn},
      {fr, f, 7} => {p, black, pawn},
      {fr, g, 7} => {p, black, pawn},
      {fr, h, 7} => {p, black, pawn},
      {fr, a, 8} => {p, black, rook},
      {fr, b, 8} => {p, black, knight},
      {fr, c, 8} => {p, black, bishop},
      {fr, d, 8} => {p, black, queen},
      {fr, e, 8} => {p, black, king},
      {fr, f, 8} => {p, black, bishop},
      {fr, g, 8} => {p, black, knight},
      {fr, h, 8} => {p, black, rook}}.



-spec frth(fr(), board()) -> square().
% @doc
% Get the square at a given file and rank

frth(FR, Board) ->
    maps:get(FR, Board, empty).



-spec mv(Src, Dest, Board) -> Result
    when Src    :: fr(),
         Dest   :: fr(),
         Board  :: board(),
         Result :: {ok, NewBoard :: board()}
                 | {error, Reason :: any()}.
% @doc
% Try to move a piece. Will return error if illegal move.

mv(SrcFR, DestFR, Board) ->
    case ce_mvj:is_legal(SrcFR, DestFR, Board) of
        legal ->
            NewBoard = really_mv(SrcFR, DestFR, Board),
            {ok, NewBoard};
        {illegal, Reasons} ->
            {error, {illegal, Reasons}}
    end.



-spec pf_board_wb(board()) -> iolist().
% @doc
% Pretty-format the board with white at the bottom.

pf_board_wb(Board) ->
    pf_board_wb(Board, {fr, h, 1}, []).



-spec pp_board_wb(board()) -> ok.
% @doc
% Pretty print the board with white at the bottom

pp_board_wb(Board) ->
    io:format("~ts~n", [pf_board_wb(Board)]).



%%% PRIVATE

-spec pf_board_wb(board(), fr(), Accum :: iolist()) -> iolist().
% @private
% Pretty format the board; this is the looper thing
% @end

% Enumerating right to left down to up (sort of in reverse, back to
% frontish)
% Beginning of the top row (terminal case)
pf_board_wb(Board, FR = {fr, a, 8}, Accum) ->
    Square = frth(FR, Board),
    SqChar = pf_square(Square),
    FinAccum = [SqChar | Accum],
    FinAccum;
% Beginning a row but not the last row, move to next row, reset column
pf_board_wb(Board, FR = {fr, a, R}, Accum) ->
    Square   = frth(FR, Board),
    SqChar   = pf_square(Square),
    % increase rank, reset file
    NewFR    = {fr, h, R + 1},
    % Add a newline
    NewAccum = [$\n, SqChar | Accum],
    % move on to next row (increase rank)
    pf_board_wb(Board, NewFR, NewAccum);
% generally, move to next column
pf_board_wb(Board, FR = {fr, F, R}, Accum) ->
    PrevFile =
        fun
            (h) -> g;
            (g) -> f;
            (f) -> e;
            (e) -> d;
            (d) -> c;
            (c) -> b;
            (b) -> a
        end,
    Square   = frth(FR, Board),
    SqChar   = pf_square(Square),
    % decrement the file (column), and leave the rank (row) intact
    NewFile  = PrevFile(F),
    NewFR    = {fr, NewFile, R},
    % add new character to beginning of accumulator
    NewAccum = [SqChar | Accum],
    pf_board_wb(Board, NewFR, NewAccum).



-spec pf_square(square()) -> integer().
% @private
% pretty format a square

pf_square({p, white, pawn})   -> $P;
pf_square({p, white, rook})   -> $R;
pf_square({p, white, knight}) -> $N;
pf_square({p, white, bishop}) -> $B;
pf_square({p, white, queen})  -> $Q;
pf_square({p, white, king})   -> $K;
pf_square({p, black, pawn})   -> $p;
pf_square({p, black, rook})   -> $r;
pf_square({p, black, knight}) -> $n;
pf_square({p, black, bishop}) -> $b;
pf_square({p, black, queen})  -> $q;
pf_square({p, black, king})   -> $k;
pf_square(empty)              -> $..



-spec really_mv(SrcFR :: fr(), DstFR :: fr(), Board :: board()) -> NewBoard :: board().
% @private
% assumes the move is legal
% set source square to empty
% set destination square to what the source square was

really_mv(SrcFR, DstFR, Board) ->
    PieceWeAreMoving = frth(SrcFR, Board),
    Board1 = maps:remove(SrcFR, Board),
    NewBoard = maps:update(DstFR, PieceWeAreMoving, Board1),
    NewBoard.



