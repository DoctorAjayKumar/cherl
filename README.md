# Vision

- service that you interact with by sending erlang terms over telnet

```
central server
    - game state
    - keeps track of whose move it is
    - checks legal moves

user story

    - connect to central server
    - get put into a game, either as white waiting for black, or as
      black paired with white
    - interact by sending messages over tcp, getting game state from
      server, until someone wins, or it's a draw
    - both sides have to agree on a draw

- start up cherg
- give it address and port of a server
- if your connection dies, or you send invalid input, you lose
- you get randomly paired up
```

# stupid things in chess

- en passant
    - only legal in the move immediately following
- castling
- knights
- dead positions

# partition of project

- cherc = chess in erlang client
    - this talks to the server
    - dependency for a gui or a web application
- cherd = chess in erlang daemon
    - server
- cherl = chess in erlang library
    - pure game state
    - dependency for both client and server

# things that are needed in the state record

https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation

1. piece placement (this is our board type)
2. whose turn it is (who goes next)
3. castling availability
4. en passant target square
5. number of halfmoves since last capture or pawn advance (for 50 move rule)
6. number of the full move (starts at 1, incremented after black's move).
